package m3uf5_practica1.model;

import static java.awt.SystemColor.text;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GestioPersona implements IGestioPersona {

    private List<Persona> persones;

    public GestioPersona() {
        
    }

    /**
     *
     * @param p
     * @throws NifNoValidException
     */
    @Override
    public void afegir(Persona p)  {          
        comprovaNif(p.nif);
       comprovaEdat(p.getDataNaixement());
      
       if (p.getNom() == null){
            try {
                throw new Exception("Campo vacio");
            } catch (Exception ex) {
                Logger.getLogger(GestioPersona.class.getName()).log(Level.SEVERE, null, ex);
            }
		}
          if (p.getNom() == ""){
            try {
                throw new Exception("Campo vacio");
            } catch (Exception ex) {
                Logger.getLogger(GestioPersona.class.getName()).log(Level.SEVERE, null, ex);
            }
          }
          if(persones.contains(p)){
            persones.add(p);
            }
      }

    @Override
    public Persona cercar(String nif)  {
         for (Persona persone : persones) {
            if(nif.equals(persone.nif)){
                 return persone;
             }
        }
    return null;}

    @Override
    public List<Persona> llistar() {
        return persones;
    }

    private boolean comprovaNif(String nif) {
        boolean bien=false;  
        if (nif == null){
            try {
                throw new Exception("NifNoValidException");
            } catch (Exception ex) {
                Logger.getLogger(GestioPersona.class.getName()).log(Level.SEVERE, null, ex);
            }
		}
          if (nif == ""){
            try {
                throw new Exception("NifNoValidException");
            } catch (Exception ex) {
                Logger.getLogger(GestioPersona.class.getName()).log(Level.SEVERE, null, ex);
            }
		}
          String patro="[0-9]{7}[A-Z]{1}";
          Pattern p =Pattern.compile(patro);
          Matcher m = p.matcher(nif);
          
          if(m.find()==true){ 
              bien=true;
          }else{
              try {
                  throw new Exception("dates incorrectes");
              } catch (Exception ex) {
                  Logger.getLogger(GestioPersona.class.getName()).log(Level.SEVERE, null, ex);
              }
          }
    return bien;}

    private boolean comprovaEdat(LocalDate data) {
        boolean comprovat=false;
        long menor=16;
        long mayor=65;
         LocalDate fHoy= LocalDate.now();
         long edad1= ChronoUnit.YEARS.between(data, fHoy);
         long edad2= ChronoUnit.YEARS.between(data, fHoy);
         if(edad1>=menor && edad2<=mayor){
             comprovat=true;
         }
        
        
        return comprovat; 
    }
   
}
