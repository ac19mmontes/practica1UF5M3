package m3uf5_practica1.model;

import java.util.logging.Logger;


public class GestioPersonaException extends Exception{  

    public GestioPersonaException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
