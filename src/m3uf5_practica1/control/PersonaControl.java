package m3uf5_practica1.control;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.String;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import m3uf5_practica1.model.DadesPersonaNoValidesException;
import m3uf5_practica1.model.GestioPersona;
import m3uf5_practica1.model.GestioPersonaException;
import m3uf5_practica1.model.Persona;
import m3uf5_practica1.vista.PersonaGUI;

public class PersonaControl implements ActionListener {

    private GestioPersona gestio;
    private PersonaGUI gui;
    
    
    
    public PersonaControl(GestioPersona gestio) {
        this.gestio = gestio;
        afegirDadesInicials();
        this.gui = new PersonaGUI(this);
        gui.setVisible(true);
    }

    /**
     * mètode de la interfície ActionListener, s'executa cada vegada que es
     * clica a un dels botons que tenen afegida com a listener l'instància
     * d'aquesta classe. (PersonaControl)
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        //recuperem la propietat actionCommand del botó sobre el que s'ha clicat
        String accio = e.getActionCommand();

        switch (accio) {
            case "Afegir":
                afegir();
                break;
            case "Cercar":
                cercar();
                break;
            case "Llistar":
                llistar();
                break;
            case "Sortir":
                System.exit(0);
        }

    }

    /**
     * accions necessàries per afegir la persona
     */
    private void afegir(){
        String nif=gui.getNifAfegir();
        String nom=gui.getNomAfegir();
        String data=gui.getDataNaixementAfegir();
        LocalDate Naixement;
        try {
            Naixement = passarStringAData(data);
            gestio.afegir(new Persona(nif,nom,Naixement));
        } catch (DadesPersonaNoValidesException ex) {
            gui.setResultatAfegir(ex.getMessage());
        }catch (GestioPersonaException ex) {
            gui.setResultatAfegir(ex.getMessage());
        }
     
            
    }

    /**
     * accions necessàries per cercar
     */
    private void cercar() {
     String nif=gui.getNifCercar();
     gestio.cercar(nif);
    }

    /**
     * accions necessàries per llistar
     */
    private void llistar() {   
         StringBuilder llista = new StringBuilder();
         for (Persona p : gestio.llistar()) {
         llista.append(p.toString()).append("\n");
       }
        gui.setLlistar(llista.toString());
    }

    private LocalDate passarStringAData(String dataText) throws DadesPersonaNoValidesException {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate data = LocalDate.parse(dataText,fmt);
        return data;
    }

    /**
     * afegeix unes quantes persones a la llista persones de GestioPersona, per 
     * tenir-ne carregades quan s'inicia l'aplicació
     */
    private void afegirDadesInicials() {
        StringBuilder llista = new StringBuilder();
        for (Persona p : gestio.llistar()){
            gestio.afegir(p);
        } 
    }

}
